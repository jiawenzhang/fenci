package cn.top.fenci.ikutil;
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;




/**
 * @Autor zhangjiawen
 * @Date: 2020/9/23 9:48
 */

public class IKSUtil {


    public static List<String> getStringList(String text) throws Exception {
        //独立Lucene实现
        StringReader re = new StringReader(text);
        IKSegmenter ik = new IKSegmenter(re, true);
        Lexeme lex;
        List<String> s = new ArrayList<>();
        while ((lex = ik.next()) != null) {
            s.add(lex.getLexemeText());
        }
        return s;

    }



    public static void main(String[] args) {
        try {
            List<String> stringList = getStringList("这只皮靴号码大了。那只号码合适。hello 啊  laozhang");
            System.out.println(stringList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}

