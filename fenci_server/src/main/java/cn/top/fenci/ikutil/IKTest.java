package cn.top.fenci.ikutil;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.imageio.stream.FileImageInputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.ResourceLoader;
import org.wltea.analyzer.cfg.Configuration;
import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;
import org.wltea.analyzer.dic.Dictionary;
import org.wltea.analyzer.lucene.IKAnalyzer;


/**
 * @Autor zhangjiawen
 * @Date: 2020/11/19 15:17
 */
public class IKTest {

    private static Dictionary singleton;
    private static ResourceLoader loader;
    static List<String> dictList = new ArrayList<String>();



    public static void main(String[] args) throws IOException {
        //test(1);
        for (int i = 0; i < 4; i++) {
            test(i);
        }

    }

    public static void test(int num) throws IOException{
        String content = "我是一个好人，他是坏人";
        StringReader input = new StringReader(content.trim());
        MyConfiguration mycfg = new MyConfiguration();
        mycfg.setUseSmart(true);  //true　用智能分词　，false细粒度

        if (singleton == null) {
            singleton = Dictionary.initial(mycfg);
        }

        String dict = null;
        if(num==0){
            dict = "org/wltea/analyzer/dic/main2012.dic";//分词jar包自带的词典
            System.out.println("加载扩展词典："+dict);
            mycfg.setMainDictionary(dict);//动态设置自定义的词库

            //在使用新词典时，清除其他词典（刷新）
            if(dictList.size()>0){
                singleton.disableWords(dictList);
            }

            //dictList:通过我们指定的词典文件获取到的词组list
            dictList = getWordList("/"+dict);
            singleton.addWords(dictList);
        }else if(num==1){
            dict = "dict.dic";
            System.out.println("加载扩展词典："+dict);
            mycfg.setMainDictionary(dict);//动态设置自定义的词库

            //在使用新词典时，清除其他词典（刷新）
            if(dictList.size()>0){
                singleton.disableWords(dictList);
            }

            dictList = getWordList(dict);
            singleton.addWords(dictList);
        }else if(num==2){
            dict = "dict2.dic";
            System.out.println("加载扩展词典："+dict);
            mycfg.setMainDictionary(dict);//动态设置自定义的词库

            //在使用新词典时，清除其他词典（刷新）
            if(dictList.size()>0){
                singleton.disableWords(dictList);
            }

            dictList = getWordList(dict);
            singleton.addWords(dictList);
        }else if(num==3){
            dict = "dict3.dic";
            System.out.println("加载扩展词典："+dict);
            mycfg.setMainDictionary(dict);//动态设置自定义的词库

            //在使用新词典时，清除其他词典（刷新）
            if(dictList.size()>0){
                singleton.disableWords(dictList);
            }

            dictList = getWordList(dict);
            singleton.addWords(dictList);
        }


        IKSegmenter ikSeg = new IKSegmenter(input,mycfg);

        Lexeme lexeme=null;
        while((lexeme=ikSeg.next())!=null){
            String keys = lexeme.getLexemeText();

            if(keys.length()>1){
                System.out.println(keys);
            }
        }
        input.close();
    }

    /**
     * 读取字典
     * @param dict
     * @return
     */
    public static List<String> getWordList(String dict) {
        List<String> list = new ArrayList<>();
        InputStream is = null;
        try {
            is = IKTest.class.getResourceAsStream(dict);
            BufferedReader br = new BufferedReader(new InputStreamReader(is,"UTF-8"), 1024);
            String theWord = null;
            do {
                theWord = br.readLine();
                if (theWord != null && !"".equals(theWord.trim())) {
                    list.add(theWord);
                }
            } while (theWord != null);
        } catch (IOException ioe) {
            System.err.println("字典文件读取失败:"+dict);
            ioe.printStackTrace();
        } finally {
            try {
                if (is != null) {
                    is.close();
                    is = null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return list;
    }





}
