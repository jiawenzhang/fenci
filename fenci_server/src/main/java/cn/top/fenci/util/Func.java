package cn.top.fenci.util;

import cn.hutool.core.util.StrUtil;

import java.util.Arrays;
import java.util.List;

import static cn.hutool.core.convert.Convert.toLong;

/**
 * @Autor zhangjiawen
 * @Date: 2020/8/20 10:38
 */
public class Func {

    public static List<Long> toLongList(String str) {
        return Arrays.asList(toLongArray(str));
    }
    public static Long[] toLongArray(String str) {
        return toLongArray(",", str);
    }
    public static String[] toStringArray(String str) {
        return toStringArray(",", str);
    }
    public static Long[] toLongArray(String split, String str) {
        if (StrUtil.isEmpty(str)) {
            return new Long[0];
        } else {
            String[] arr = str.split(split);
            Long[] longs = new Long[arr.length];

            for(int i = 0; i < arr.length; ++i) {
                Long v = toLong(arr[i], 0L);
                longs[i] = v;
            }

            return longs;
        }
    }


    public static String[] toStringArray(String split, String str) {
        if (StrUtil.isEmpty(str)) {
            return new String[0];
        } else {
            String[] arr = str.split(split);
            String[] strings = new String[arr.length];

            for(int i = 0; i < arr.length; ++i) {
                String v = arr[i];
                strings[i] = v;
            }

            return strings;
        }
    }
}


