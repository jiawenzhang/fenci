package cn.top.fenci.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 统一返回对象
 *
 * @author zhangjiawen
 * @date 2020/07/21 10:24
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResult<T> {

    private Integer code;
    private String message;
    private T data;

    public CommonResult(Integer code, String message) {
        this(code, message, null);
    }
    public CommonResult(T data) {
        this(200, "success", data);
    }
    public static CommonResult failed(){
        return new CommonResult(-100,"failed");
    }
    public static CommonResult failed(String msg){
        return new CommonResult(-100,msg);
    }

    public static CommonResult succeed(){
        return new CommonResult(200,"succeed");
    }
}
