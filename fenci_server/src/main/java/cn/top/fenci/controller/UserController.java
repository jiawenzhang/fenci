package cn.top.fenci.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import cn.top.fenci.entity.ImFriends;
import cn.top.fenci.entity.ImGroup;
import cn.top.fenci.entity.ImUser;
import cn.top.fenci.service.IImFriendsService;
import cn.top.fenci.service.IImGroupService;
import cn.top.fenci.service.IImUserService;
import cn.top.fenci.util.CommonResult;
import cn.top.fenci.vo.InitUser;
import cn.top.fenci.vo.InitUserAndFriends;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-07-21
 */
@CrossOrigin
@RestController
@Slf4j
@Api(tags = "创建聊天窗口相关接口")
public class UserController {
    /**
     * 根据用户id查询好友列表
     * @return
     */
    @Resource
    private IImUserService userService;
    @Resource
    private IImFriendsService friendsService;
    @Value("${default.img-url}")
    private String defaultImg;
    @Resource
    private IImGroupService groupService;







    @PostMapping("/addUser")
    @ApiOperation(value = "批量添加用户", notes = "必要的参数：List<InitUser>")
    public CommonResult postIM(@RequestBody List<InitUser> initUserList){

        if (initUserList==null || initUserList.size()==0){
            return new CommonResult(-100,"列表为空！");
        }
        for (InitUser initUser:initUserList) {
            if (StrUtil.isBlank(initUser.getUserId())||StrUtil.isBlank(initUser.getUserName())||StrUtil.isBlank(initUser.getGroupId())
                    ||StrUtil.isBlank(initUser.getGroupName())){
                log.warn(" 存在用户缺少必要的参数！:"+initUser.toString());
                return new CommonResult(-100,"存在用户缺少必要的参数！");
            }

            // log.info("登录id:"+initUser.getUserId()+"name:"+initUser.getUserName());
            ImUser imUser =null;
            //查询相关用户是否存在
            imUser = userService.getUserByOutUserAndGoupCode(initUser.getGroupId(), initUser.getUserId());
            //不存在则添加
            if (imUser==null){
                //添加机构
                ImGroup group =null;
                group = groupService.getOne(Wrappers.<ImGroup>query().lambda().eq(ImGroup::getStrGroupCode, initUser.getGroupId()));
                if (group==null){
                    ImGroup imGroup=new ImGroup();
                    imGroup.setStrGroupCode(initUser.getGroupId());
                    imGroup.setStrGroupName(initUser.getGroupName());
                    imGroup.setStrCompanyName(initUser.getGroupName());
                    imGroup.setDtCreatTime(new Date());
                    imGroup.setDtUpdateTime(new Date());
                    groupService.save(imGroup);
                    group=new ImGroup();
                    BeanUtil.copyProperties(imGroup,group);
                }
                //添加用户
                ImUser newUser=new ImUser();
                newUser.setStrUnionUserId(initUser.getUserId());
                newUser.setStrNickName(initUser.getUserName());
                newUser.setStrUserName(initUser.getUserName());
                newUser.setStrGroupId(Convert.toStr(group.getLId()));
                newUser.setStrMobile(StrUtil.isBlank(initUser.getMobile())?"":initUser.getMobile());
                newUser.setStrPosition(StrUtil.isBlank(initUser.getPosition())?"":initUser.getPosition());
                newUser.setStrImgUrl(defaultImg);
                newUser.setStrtHumbnailUrl(defaultImg);
                newUser.setDtCreateTime(new Date());
                newUser.setDtUpateTime(new Date());
                newUser.setNStatus(0);
                userService.save(newUser);
                imUser=new ImUser();
                BeanUtil.copyProperties(newUser,imUser);
            }

        }
        return CommonResult.succeed();
    }



    //TODO  初始化用户及相关好友信息
    @PostMapping("/AddUserAndFriends")
    @ApiOperation(value = "批量添加用户及好友信息", notes = "必要的参数：List<InitUserAndFriends>")

    public CommonResult AddUserAndFriends(@RequestBody List<InitUserAndFriends> list){

        if (list.size()==0){
            return CommonResult.failed("参数缺失！");
        }
        list.forEach(initUserAndFriends->{
            ImGroup userGroup, friendGroup;
            userGroup = groupService.getOne(Wrappers.<ImGroup>query().lambda()
                    .eq(ImGroup::getStrGroupCode, initUserAndFriends.getGroupId()));
            friendGroup = groupService.getOne(Wrappers.<ImGroup>query().lambda()
                    .eq(ImGroup::getStrGroupCode, initUserAndFriends.getFriendGroupId()));

            if (userGroup==null){
                ImGroup imGroup=new ImGroup();
                imGroup.setDtCreatTime(new Date());
                imGroup.setDtUpdateTime(new Date());
                imGroup.setStrCompanyName(initUserAndFriends.getGroupName());
                imGroup.setStrGroupName(initUserAndFriends.getGroupName());
                imGroup.setStrGroupCode(initUserAndFriends.getGroupId());
                imGroup.setNIsDeleted(0);
                groupService.save(imGroup);
                userGroup=imGroup;

            }else {
                userGroup.setDtUpdateTime(new Date());
                userGroup.setStrCompanyName(initUserAndFriends.getGroupName());
                userGroup.setStrGroupName(initUserAndFriends.getGroupName());
                groupService.updateById(userGroup);
            }

            if (friendGroup==null){
                ImGroup imGroup=new ImGroup();
                imGroup.setDtCreatTime(new Date());
                imGroup.setDtUpdateTime(new Date());
                imGroup.setStrCompanyName(initUserAndFriends.getFriendGroupName());
                imGroup.setStrGroupName(initUserAndFriends.getFriendGroupName());
                imGroup.setStrGroupCode(initUserAndFriends.getFriendGroupId());
                imGroup.setNIsDeleted(0);
                groupService.save(imGroup);
                friendGroup=imGroup;

            }else {
                friendGroup.setDtUpdateTime(new Date());
                friendGroup.setStrCompanyName(initUserAndFriends.getFriendGroupName());
                friendGroup.setStrGroupName(initUserAndFriends.getFriendGroupName());
                groupService.updateById(friendGroup);
            }


            ImUser user,friend;
            user=userService.getOne(Wrappers.<ImUser>query().lambda().eq(ImUser::getStrUnionUserId,initUserAndFriends.getUserId())
                    .eq(ImUser::getStrGroupId,userGroup.getLId()));
            friend=userService.getOne(Wrappers.<ImUser>query().lambda().eq(ImUser::getStrUnionUserId,initUserAndFriends.getFriendId())
                    .eq(ImUser::getStrGroupId,friendGroup.getLId()));
            if (user==null){
                ImUser imUser=new ImUser();
                imUser.setStrUnionUserId(initUserAndFriends.getUserId());
                imUser.setStrNickName(initUserAndFriends.getUserName());
                imUser.setStrUserName(initUserAndFriends.getUserName());
                imUser.setStrGroupId(Convert.toStr(userGroup.getLId()));
                imUser.setStrMobile(StrUtil.isBlank(initUserAndFriends.getMobile())?"":initUserAndFriends.getMobile());
                imUser.setStrPosition(StrUtil.isBlank(initUserAndFriends.getPosition())?"":initUserAndFriends.getPosition());
                imUser.setStrImgUrl(defaultImg);
                imUser.setStrtHumbnailUrl(defaultImg);
                imUser.setStrPosition(initUserAndFriends.getPosition());
                imUser.setDtCreateTime(new Date());
                imUser.setDtUpateTime(new Date());
                imUser.setNStatus(0);
                userService.save(imUser);
                user=imUser;
            }else {
                user.setStrNickName(initUserAndFriends.getUserName());
                user.setStrUserName(initUserAndFriends.getUserName());
                user.setStrMobile(StrUtil.isBlank(initUserAndFriends.getMobile())?"":initUserAndFriends.getMobile());
                user.setStrPosition(StrUtil.isBlank(initUserAndFriends.getPosition())?"":initUserAndFriends.getPosition());
                user.setDtUpateTime(new Date());
                userService.updateById(user);
            }

            if (friend==null){
                ImUser imUser=new ImUser();
                imUser.setStrUnionUserId(initUserAndFriends.getFriendId());
                imUser.setStrNickName(initUserAndFriends.getFriendName());
                imUser.setStrUserName(initUserAndFriends.getFriendName());
                imUser.setStrGroupId(Convert.toStr(friendGroup.getLId()));
                imUser.setStrMobile(StrUtil.isBlank(initUserAndFriends.getFriendMobile())?"":initUserAndFriends.getFriendMobile());
                imUser.setStrPosition(StrUtil.isBlank(initUserAndFriends.getFriendPosition())?"":initUserAndFriends.getFriendPosition());
                imUser.setStrImgUrl(defaultImg);
                imUser.setStrtHumbnailUrl(defaultImg);
                imUser.setDtCreateTime(new Date());
                imUser.setDtUpateTime(new Date());
                imUser.setNStatus(0);
                userService.save(imUser);
                friend=imUser;
            }else {
                friend.setStrNickName(initUserAndFriends.getFriendName());
                friend.setStrUserName(initUserAndFriends.getFriendName());
                friend.setStrMobile(StrUtil.isBlank(initUserAndFriends.getFriendMobile())?"":initUserAndFriends.getFriendMobile());
                friend.setStrPosition(StrUtil.isBlank(initUserAndFriends.getFriendPosition())?"":initUserAndFriends.getFriendPosition());
                friend.setDtUpateTime(new Date());
                userService.updateById(friend);
            }

            //互加好友
            ImFriends userFriend = friendsService.getOne(Wrappers.<ImFriends>query().lambda()
                    .eq(ImFriends::getLUserId, user.getLId())
                    .eq(ImFriends::getLFriendId, friend.getLId()));
            ImFriends friendFriend = friendsService.getOne(Wrappers.<ImFriends>query().lambda()
                    .eq(ImFriends::getLUserId, friend.getLId())
                    .eq(ImFriends::getLFriendId, user.getLId()));
            if (userFriend==null){
                ImFriends newFriend=new ImFriends();
                newFriend.setLUserId(Convert.toStr(user.getLId()));
                newFriend.setLFriendId(Convert.toStr(friend.getLId()));
                newFriend.setLFriendGroupId(Convert.toStr(friendGroup.getLId()));
                newFriend.setStrFriendGroupName(friendGroup.getStrGroupName());
                newFriend.setStrFriendName(friend.getStrUserName());
                newFriend.setStrFriendNickName(friend.getStrNickName());
                newFriend.setStrImgUrl(friend.getStrImgUrl());
                newFriend.setStrtHumbnailUrl(friend.getStrtHumbnailUrl());
                newFriend.setStrFriendMoblie(friend.getStrMobile());
                newFriend.setStrPosition(friend.getStrPosition());
                friendsService.save(newFriend);
            }else{
                userFriend.setStrFriendGroupName(friendGroup.getStrGroupName());
                userFriend.setStrFriendName(friend.getStrUserName());
                userFriend.setStrFriendNickName(friend.getStrNickName());
                userFriend.setStrImgUrl(friend.getStrImgUrl());
                userFriend.setStrPosition(friend.getStrPosition());
                friendsService.updateById(userFriend);
            }

            if (friendFriend==null){
                ImFriends newFriend=new ImFriends();
                newFriend.setLUserId(Convert.toStr(friend.getLId()));
                newFriend.setLFriendId(Convert.toStr(user.getLId()));
                newFriend.setLFriendGroupId(Convert.toStr(userGroup.getLId()));
                newFriend.setStrFriendGroupName(userGroup.getStrGroupName());
                newFriend.setStrFriendName(user.getStrUserName());
                newFriend.setStrFriendNickName(user.getStrNickName());
                newFriend.setStrImgUrl(user.getStrImgUrl());
                newFriend.setStrtHumbnailUrl(user.getStrtHumbnailUrl());
                newFriend.setStrFriendMoblie(user.getStrMobile());
                newFriend.setStrPosition(user.getStrPosition());
                friendsService.save(newFriend);
            }else{
                friendFriend.setStrFriendGroupName(userGroup.getStrGroupName());
                friendFriend.setStrFriendName(user.getStrUserName());
                friendFriend.setStrFriendNickName(user.getStrNickName());
                friendFriend.setStrFriendMoblie(user.getStrMobile());
                friendFriend.setStrPosition(user.getStrPosition());
                friendsService.updateById(friendFriend);
            }
        });




        return  CommonResult.succeed();
    }

}

