package cn.top.fenci.controller;

import cn.hutool.core.util.StrUtil;
import cn.top.fenci.service.JwtService;
import cn.top.fenci.vo.InitUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * 获取jwt令牌服务
 * @Autor zhangjiawen
 * @Date: 2020/10/22 13:33
 */
@RestController
@CrossOrigin
@RequestMapping("/jwt")
@Api(tags = "生成jwt令牌接口")
public class JwtContorller {
    @Resource
    private JwtService jwtService;


    @PostMapping("/genToken")
    @ApiOperation(value = "生成jwt令牌接口",notes = "")
    public String genToken(@RequestBody  InitUser initUser){
        if (StrUtil.isBlank(initUser.getUserId())||StrUtil.isBlank(initUser.getUserName())||StrUtil.isBlank(initUser.getGroupId())
                ||StrUtil.isBlank(initUser.getGroupName())||StrUtil.isBlank(initUser.getMobile())){

            return "缺少必要的参数！";
        }
        Map<String,String> map=new HashMap();
        map.put("userId",initUser.getUserId());
        map.put("userName",initUser.getUserName());
        map.put("mobile",initUser.getMobile());
        map.put("groupId",initUser.getGroupId());
        map.put("groupName",initUser.getGroupName());
        if (StrUtil.isNotBlank(initUser.getPosition())){
            map.put("position",initUser.getPosition());
        }

        if (StrUtil.isNotBlank(initUser.getInitAsk())){
            map.put("initAsk",initUser.getInitAsk());
        }
        if (StrUtil.isNotBlank(initUser.getToUserId())){
            map.put("toUserId",initUser.getToUserId());
        }
        if (StrUtil.isNotBlank(initUser.getToGroupId())){
            map.put("toGroupId",initUser.getToGroupId());
        }
        return jwtService.genJwt(map);
    }
}
