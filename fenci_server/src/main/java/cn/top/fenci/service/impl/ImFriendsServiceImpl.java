package cn.top.fenci.service.impl;

import cn.top.fenci.entity.ImFriends;
import cn.top.fenci.mapper.ImFriendsMapper;
import cn.top.fenci.service.IImFriendsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-07-21
 */
@Service
public class ImFriendsServiceImpl extends ServiceImpl<ImFriendsMapper, ImFriends> implements IImFriendsService {
    @Resource
    private ImFriendsMapper friendsMapper;
    @Override
    public List<String> getAllFriendIdById(String userId) {
        return friendsMapper.getAllFriendIdById(userId);
    }
}
