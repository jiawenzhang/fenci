package cn.top.fenci.service;

import cn.top.fenci.entity.ImUser;
import cn.top.fenci.vo.ImUserVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-07-21
 */
public interface IImUserService extends IService<ImUser> {

    public List<ImUser> getUsersByGroupId(String groupId);
    public List<ImUserVo> getAllUserInfo(String userId);
    public List<ImUserVo> getAllUserInfoByIds(List<String> ids);
    public ImUser getUserByOutUserAndGoupCode(String strGroupCode, String strUnionUserId);
}
