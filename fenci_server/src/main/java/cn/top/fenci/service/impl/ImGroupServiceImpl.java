package cn.top.fenci.service.impl;

import cn.top.fenci.entity.ImGroup;
import cn.top.fenci.mapper.ImGroupMapper;
import cn.top.fenci.service.IImGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-08-06
 */
@Service
public class ImGroupServiceImpl extends ServiceImpl<ImGroupMapper, ImGroup> implements IImGroupService {

}
