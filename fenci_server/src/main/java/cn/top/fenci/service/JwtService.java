package cn.top.fenci.service;

import java.util.Map;

/**生成jwttoken
 * @Autor zhangjiawen
 * @Date: 2020/10/22 13:18
 */
public interface JwtService {
    String genJwt(Map<String, String> claims);
}
