package cn.top.fenci.service;

import cn.top.fenci.entity.ImFriends;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-07-21
 */
public interface IImFriendsService extends IService<ImFriends> {
    List<String> getAllFriendIdById(String userId);
}
