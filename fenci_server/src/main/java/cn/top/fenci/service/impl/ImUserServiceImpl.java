package cn.top.fenci.service.impl;

import cn.hutool.core.convert.Convert;
import cn.top.fenci.entity.ImUser;
import cn.top.fenci.mapper.ImUserMapper;
import cn.top.fenci.service.IImUserService;
import cn.top.fenci.vo.ImUserVo;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-07-21
 */
@Service
public class ImUserServiceImpl extends ServiceImpl<ImUserMapper, ImUser> implements IImUserService {
    @Resource
    private ImUserMapper userMapper;

    @Override
    public List<ImUser> getUsersByGroupId(String groupId) {
        return userMapper.getUsersByGroupId(groupId);
    }

    @Override
    public List<ImUserVo> getAllUserInfo(String userId) {
        List<ImUserVo> allUserInfo = userMapper.getAllUserInfo(userId);
        allUserInfo.forEach(userInfo->{
            userInfo.setStrId(Convert.toStr(userInfo.getStrId()));
        });
        return allUserInfo;
    }

    @Override
    public List<ImUserVo> getAllUserInfoByIds(List<String> ids) {
        return userMapper.getAllUserInfoByIds(ids);
    }

    @Override
    public ImUser getUserByOutUserAndGoupCode(String strGroupCode, String strUnionUserId) {
       return userMapper.getUserByOutUserAndGoupCode(strGroupCode,strUnionUserId);
    }


}
