package cn.top.fenci.service;

import cn.top.fenci.entity.ImGroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-08-06
 */
public interface IImGroupService extends IService<ImGroup> {

}
