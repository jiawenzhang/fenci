package cn.top.fenci.service.impl;

import cn.top.fenci.service.JwtService;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * @Autor zhangjiawen
 * @Date: 2020/10/22 13:20
 */
@Service
public class JwtServiceImpl implements JwtService {

    private static final String SECRET = "t1o2k3e4n5_s9e8c7r6e5t";
    private static final String ISSUER = "star_jwt";
    @Value("${token.work-time}")
    private Integer workingTime;
    @Override
    public String genJwt(Map<String, String> claims) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(SECRET);
            JWTCreator.Builder builder = JWT.create().withIssuer(ISSUER).withExpiresAt(DateUtils.addMinutes(new Date(),workingTime));
            claims.forEach(builder::withClaim);
            return builder.sign(algorithm);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
