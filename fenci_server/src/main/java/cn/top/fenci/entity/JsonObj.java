package cn.top.fenci.entity;

/**
 * @Autor zhangjiawen
 * @Date: 2020/8/5 16:35
 */
public class JsonObj {
    private int state;
    private String msg;
    private int msgType;
    private String zipMsg;


    public int getMsgType() {
        return msgType;
    }

    public void setMsgType(int msgType) {
        this.msgType = msgType;
    }

    public String getZipMsg() {
        return zipMsg;
    }

    public void setZipMsg(String zipMsg) {
        this.zipMsg = zipMsg;
    }

    public int getState() {
        return state;
    }
    public void setState(int state) {
        this.state = state;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public JsonObj(int state, String msg) {
        super();
        this.state = state;
        this.msg = msg;
    }
    public JsonObj(int state, String msg,String zipMsg,int msgType) {
        super();
        this.state = state;
        this.msg = msg;
        this.zipMsg=zipMsg;
        this.msgType=msgType;
    }
    public JsonObj(int state, String msg,String zipMsg) {
        super();
        this.state = state;
        this.msg = msg;
        this.zipMsg=zipMsg;
    }


}
