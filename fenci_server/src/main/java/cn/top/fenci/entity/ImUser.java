package cn.top.fenci.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-07-21
 */
@Data
  @EqualsAndHashCode(callSuper = false)
  @Accessors(chain = true)
@ApiModel(value="ImUser对象", description="")
public class ImUser implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "lId", type = IdType.ID_WORKER)
      private Long lId;

      @ApiModelProperty(value = "用户编码")
      @TableField("strNumber")
    private String strNumber;

      @ApiModelProperty(value = "昵称")
      @TableField("strNickName")
    private String strNickName;

      @ApiModelProperty(value = "姓名")
      @TableField("strUserName")
    private String strUserName;

      @ApiModelProperty(value = "电话")
      @TableField("strMobile")
    private String strMobile;

      @ApiModelProperty(value = "用户类型：0局端(用户) 1服务端(客服)")
      @TableField("nUserType")
    private Integer nUserType;

      @ApiModelProperty(value = "联合用户id（联合系统用户id）")
      @TableField("strUnionUserId")
    private String strUnionUserId;

      @ApiModelProperty(value = "在线状态 0离线 1在线")
      @TableField("nStatus")
    private Integer nStatus;

      @ApiModelProperty(value = "所属公司唯一编码")
      @TableField("strGroupId")
    private String strGroupId;

    @ApiModelProperty(value = "头像地址")
    @TableField("strImgUrl")
    private String strImgUrl;


    @ApiModelProperty(value = "头像缩略地址")
    @TableField("strtHumbnailUrl")
    private String strtHumbnailUrl;

    @ApiModelProperty(value = "职位")
    @TableField("strPosition")
    private String strPosition;

    @TableField("dtCreateTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone="GMT+8")
    private Date dtCreateTime;

    @TableField("dtUpateTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone="GMT+8")
    private Date dtUpateTime;


      @ApiModelProperty(value = "是否删除0否1是")
      @TableField("nIsDeleted")
      @TableLogic
    private Integer nIsDeleted;


    public Date getDtCreateTime() {
        if(dtCreateTime == null) {
            return null;
        }
        return (Date) dtCreateTime.clone();
    }

    public void setDtCreateTime(Date dtCreateTime) {
        if(dtCreateTime == null) {
            this.dtCreateTime = null;
        }else {
            this.dtCreateTime = (Date) dtCreateTime.clone();
        }
    }

    public Date getDtUpateTime() {
        if(dtUpateTime == null) {
            return null;
        }
        return (Date) dtUpateTime.clone();
    }

    public void setDtUpateTime(Date dtUpateTime) {
        if(dtUpateTime == null) {
            this.dtUpateTime = null;
        }else {
            this.dtUpateTime = (Date) dtUpateTime.clone();
        }
    }
}
