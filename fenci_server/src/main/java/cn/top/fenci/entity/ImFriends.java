package cn.top.fenci.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-07-21
 */
@Data
  @EqualsAndHashCode(callSuper = false)
  @Accessors(chain = true)
@ApiModel(value="ImFriends对象", description="")
public class ImFriends implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "lId", type = IdType.ID_WORKER)
      private Long lId;

      @ApiModelProperty(value = "用户id")
      @TableField("lUserId")
    private String lUserId;

      @ApiModelProperty(value = "好友id")
      @TableField("lFriendId")
    private String lFriendId;

    @ApiModelProperty(value = "好友id")
    @TableField("nOnlineState")
    private Integer nOnlineState;


      @ApiModelProperty(value = "好友名字")
      @TableField("strFriendName")
    private String strFriendName;

      @ApiModelProperty(value = "好友昵称")
      @TableField("strFriendNickName")
    private String strFriendNickName;
    @ApiModelProperty(value = "好友头像地址")
    @TableField("strImgUrl")
    private String strImgUrl;


    @ApiModelProperty(value = "头像缩略地址")
    @TableField("strtHumbnailUrl")
    private String strtHumbnailUrl;

      @ApiModelProperty(value = "好友电话")
      @TableField("strFriendMoblie")
    private String strFriendMoblie;

      @ApiModelProperty(value = "好友所属组织id")
      @TableField("lFriendGroupId")
    private String lFriendGroupId;

      @ApiModelProperty(value = "好友所属组织名称")
      @TableField("strFriendGroupName")
    private String strFriendGroupName;

    @ApiModelProperty(value = "职位")
    @TableField("strPosition")
    private String strPosition;


}
