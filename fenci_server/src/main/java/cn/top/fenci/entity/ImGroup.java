package cn.top.fenci.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-08-06
 */
@Data
@ApiModel(value="ImGroup对象", description="")
public class ImGroup implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "lId", type = IdType.ID_WORKER)
      private Long lId;

    @TableField("strGroupCode")
    private String strGroupCode;

    @TableField("strGroupName")
    private String strGroupName;

    @TableField("strCompanyName")
    private String strCompanyName;


    @TableField("dtCreatTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone="GMT+8")
    private Date dtCreatTime;

    @TableField("dtUpdateTime")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss" ,timezone="GMT+8")
    private Date dtUpdateTime;

    @ApiModelProperty(value = "是否删除0否1是")
    @TableField("nIsDeleted")
    @TableLogic
    private Integer nIsDeleted;


    public Date getDtCreatTime() {
        if(this.dtCreatTime == null) {
            return null;
        }
        return (Date) this.dtCreatTime.clone();
    }

    public void setDtCreatTime(Date dtCreatTime) {
        if(dtCreatTime == null) {
            this.dtCreatTime = null;
        }else {
            this.dtCreatTime = (Date) dtCreatTime.clone();
        }
    }

    public Date getDtUpdateTime() {
        if(this.dtUpdateTime == null) {
            return null;
        }
        return (Date) this.dtUpdateTime.clone();
    }

    public void setDtUpdateTime(Date dtUpdateTime) {
        if(dtUpdateTime == null) {
            this.dtUpdateTime = null;
        }else {
            this.dtUpdateTime = (Date) dtUpdateTime.clone();
        }
    }


}
