package cn.top.fenci.mapper;

import cn.top.fenci.entity.ImUser;
import cn.top.fenci.vo.ImUserVo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-07-21
 */
public interface ImUserMapper extends BaseMapper<ImUser> {

    public List<ImUser> getUsersByGroupId(String groupId);

    @Select("SELECT u.*,g.strGroupName FROM im_user u LEFT JOIN im_group g ON u.strGroupId=g.lId WHERE u.nIsDeleted=0 AND u.lId!=#{userId}")
    public List<ImUserVo> getAllUserInfo(String userId);

    @Select({
            "<script>",
            "SELECT u.*,g.strGroupName FROM im_user u LEFT JOIN im_group g ON u.strGroupId=g.lId ",
            "WHERE u.nIsDeleted=0 AND u.lId IN ",
            "<foreach item='item' index='index' collection='ids'",
            "open='(' separator=',' close=')'>",
            "#{item}",
            "</foreach>",
            "</script>"
    })
    public List<ImUserVo> getAllUserInfoByIds(@Param("ids") List<String> ids);

    @Select("SELECT u.* FROM im_user u LEFT JOIN im_group g on u.strGroupId=g.lId WHERE  u.nIsDeleted=0 AND g.strGroupCode=#{strGroupCode} and u.strUnionUserId=#{strUnionUserId} ")
    public ImUser getUserByOutUserAndGoupCode(String strGroupCode, String strUnionUserId);



}
