package cn.top.fenci.mapper;

import cn.top.fenci.entity.ImFriends;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-07-21
 */
public interface ImFriendsMapper extends BaseMapper<ImFriends> {

     List<String> getAllFriendIdById(String userId);
}
