package cn.top.fenci.mapper;

import cn.top.fenci.entity.ImGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zhangjiawen
 * @since 2020-08-06
 */
public interface ImGroupMapper extends BaseMapper<ImGroup> {

}
