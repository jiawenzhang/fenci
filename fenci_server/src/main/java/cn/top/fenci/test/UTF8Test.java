package cn.top.fenci.test;


import java.io.UnsupportedEncodingException;

/**
 * @Autor zhangjiawen
 * @Date: 2020/11/20 13:53
 */
public class UTF8Test {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String str="中国人";
        System.out.println(new String(str.getBytes("utf-8")));
        String gs=new String();
        String gbkStr=new String(str.getBytes("ISO-8859-1"));
        System.out.println(gbkStr);
        String s = new String(gbkStr.getBytes("utf-8"));
        System.out.println(new String(s.getBytes("utf-8"),"ISO-8859-1"));
    }
}
