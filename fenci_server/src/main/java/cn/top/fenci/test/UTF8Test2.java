package cn.top.fenci.test;


import java.io.UnsupportedEncodingException;

/**
 * @Autor zhangjiawen
 * @Date: 2020/11/20 13:53
 */
public class UTF8Test2 {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String str="???";
        System.out.println(str);
        System.out.println(new String(str.getBytes("utf-8")));

        System.out.println(new String(str.getBytes("utf-8"),"ISO-8859-1"));
    }
}
