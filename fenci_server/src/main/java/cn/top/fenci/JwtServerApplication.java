package cn.top.fenci;


import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Autor zhangjiawen
 * @Date: 2020/7/21 9:07
 */
@SpringBootApplication
@MapperScan("cn.top.jwt.mapper")
public class JwtServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(JwtServerApplication.class);
    }


}
