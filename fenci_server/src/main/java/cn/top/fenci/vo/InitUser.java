package cn.top.fenci.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Autor zhangjiawen
 * @Date: 2020/10/21 11:37
 */
@Data
@ApiModel(value = "生成令牌参数", description = "传入必要参数生成令牌")
public class InitUser {
    @ApiModelProperty(value = "用户id（必填）")
    private String userId;
    @ApiModelProperty(value = "用户名（必填）")
    private String userName;
    @ApiModelProperty(value = "手机（非必填）")
    private String mobile;
    @ApiModelProperty(value = "机构id（必填）")
    private String groupId;
    @ApiModelProperty(value = "机构名称（必填）")
    private String groupName;
    @ApiModelProperty(value = "职位（非必填）")
    private String position;
    @ApiModelProperty(value = "非必填")
    private String initAsk;

    private String toUserId;
    private String toGroupId;


}
