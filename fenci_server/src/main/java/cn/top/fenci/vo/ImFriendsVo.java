package cn.top.fenci.vo;


import cn.top.fenci.entity.ImFriends;

/**
 * @Autor zhangjiawen
 * @Date: 2020/9/3 10:37
 */
public class ImFriendsVo extends ImFriends {

    private String strId;

    public ImFriendsVo() {
    }

    public String getStrId() {
        return this.strId;
    }

    public void setStrId(final String strId) {
        this.strId = strId;
    }

    @Override
    public String toString() {
        return "ImUserVo(strId=" + this.getStrId() + ")";
    }

    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (!(o instanceof ImFriendsVo)) {
            return false;
        } else {
            ImFriendsVo other = (ImFriendsVo)o;
            if (!other.canEqual(this)) {
                return false;
            } else if (!super.equals(o)) {
                return false;
            } else {
                Object this$strId = this.getStrId();
                Object other$strId = other.getStrId();
                if (this$strId == null) {
                    if (other$strId != null) {
                        return false;
                    }
                } else if (!this$strId.equals(other$strId)) {
                    return false;
                }

                return true;
            }
        }
    }

    @Override
    protected boolean canEqual(final Object other) {
        return other instanceof ImFriendsVo;
    }

    @Override
    public int hashCode() {
        int PRIME = 1;
        int result = super.hashCode();
        Object $strId = this.getStrId();
        result = result * 59 + ($strId == null ? 43 : $strId.hashCode());
        return result;
    }
}
