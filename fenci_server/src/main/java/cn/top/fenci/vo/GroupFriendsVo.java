package cn.top.fenci.vo;




import cn.top.fenci.entity.ImFriends;

import java.util.ArrayList;
import java.util.List;

/**
 * @Autor zhangjiawen
 * @Date: 2020/8/11 11:36
 */


public class GroupFriendsVo {

    private String groupId;
    private String groupName;
    private List<ImFriends> friendsList;

    public GroupFriendsVo(String groupId, String groupName) {
        this.groupId = groupId;
        this.groupName = groupName;

    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<ImFriends> getFriendsList() {
        if (this.friendsList == null) {
            this.friendsList = new ArrayList<>();
        }
        return friendsList;
    }

    public void setFriendsList(List<ImFriends> friendsList) {
        this.friendsList = friendsList;
    }

    @Override
    public boolean equals(Object obj) {
        if (null == obj ) {
            return false;
        }

        if(obj instanceof GroupFriendsVo) {
            GroupFriendsVo other = (GroupFriendsVo) obj;
            if (this.getGroupId().equals(other.getGroupId())) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        return this.getGroupId().hashCode();
    }
}
