package cn.top.fenci.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Autor zhangjiawen
 * @Date: 2020/10/21 11:37
 */
@Data
@ApiModel(value = "初始化用户及好友实体类", description = "初始化用户及好友实体类")
public class InitUserAndFriends {
    @ApiModelProperty(value = "用户id（必填）",required = true)
    private String userId;
    @ApiModelProperty(value = "用户名（必填）",required = true)
    private String userName;
    @ApiModelProperty(value = "机构id（必填）",required = true)
    private String groupId;
    @ApiModelProperty(value = "机构名称（必填）",required = true)
    private String groupName;
    @ApiModelProperty(value = "职位（必填）",required = true)
    private String position;
    @ApiModelProperty(value = "手机（非必填）")
    private String mobile;
    @ApiModelProperty(value = "好友id（必填）",required = true)
    private String friendId;
    @ApiModelProperty(value = "好友姓名（必填）",required = true)
    private String friendName;
    @ApiModelProperty(value = "好友机构id（必填）",required = true)
    private String friendGroupId;
    @ApiModelProperty(value = "好友机构名称（必填）",required = true)
    private String friendGroupName;
    @ApiModelProperty(value = "好友职位（必填）",required = true)
    private String friendPosition;
    @ApiModelProperty(value = "好友手机（非必填）")
    private String friendMobile;


}
